package com.textanalyzer.textanalyzer.model;

import java.util.HashMap;

public class Consonants {
    private final HashMap<String, Integer> consonants;

    public Consonants(HashMap<String, Integer> consonants) {
        this.consonants = consonants;
    }

    public Consonants() {
        this.consonants = new HashMap<>();
    }

    public HashMap<String, Integer> getConsonants() {
        return consonants;
    }
}
