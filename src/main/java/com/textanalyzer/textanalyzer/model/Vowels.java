package com.textanalyzer.textanalyzer.model;

public class Vowels {
    private final int a;
    private final int e;
    private final int i;
    private final int o;
    private final int u;

    public Vowels(int a, int e, int i, int o, int u) {
        this.a = a;
        this.e = e;
        this.i = i;
        this.o = o;
        this.u = u;
    }

    public Vowels() {
        this.a = 0;
        this.e = 0;
        this.i = 0;
        this.o = 0;
        this.u = 0;
    }

    public int getA() {
        return a;
    }

    public int getE() {
        return e;
    }

    public int getI() {
        return i;
    }

    public int getO() {
        return o;
    }

    public int getU() {
        return u;
    }
}
