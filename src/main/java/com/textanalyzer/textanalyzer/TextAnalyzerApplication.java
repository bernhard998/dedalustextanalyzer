package com.textanalyzer.textanalyzer;

import com.textanalyzer.textanalyzer.exception.TextAnalyzerException;
import com.textanalyzer.textanalyzer.model.Consonants;
import com.textanalyzer.textanalyzer.model.Vowels;
import com.textanalyzer.textanalyzer.service.TextAnalyzerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class TextAnalyzerApplication {
    private final TextAnalyzerService textAnalyzerService = new TextAnalyzerService();
    private Logger logger = LoggerFactory.getLogger(TextAnalyzerApplication.class);
    private final String originURL = "http://localhost:4200";

    public static void main(String[] args) {
        SpringApplication.run(TextAnalyzerApplication.class, args);
    }

    @CrossOrigin(origins = originURL)
    @GetMapping("vowels")
    public Vowels getVowels(@RequestParam(value = "text") String text) {
        try {
            return textAnalyzerService.getVowels(text);
        } catch (TextAnalyzerException e) {
            logger.error("Could not get Vowels", e);
            return new Vowels();
        }
    }

    @CrossOrigin(origins = originURL)
    @GetMapping("consonants")
    public Consonants getConsonants(@RequestParam(value = "text") String text) {
        try {
            return textAnalyzerService.getConsonants(text);
        } catch (TextAnalyzerException e) {
            logger.error("Could not get Consonants", e);
            return new Consonants();
        }
    }

}
