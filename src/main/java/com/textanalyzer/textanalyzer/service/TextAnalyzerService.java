package com.textanalyzer.textanalyzer.service;

import com.textanalyzer.textanalyzer.exception.TextAnalyzerException;
import com.textanalyzer.textanalyzer.model.Consonants;
import com.textanalyzer.textanalyzer.model.Vowels;

import java.util.HashMap;
import java.util.Optional;

public class TextAnalyzerService {
    public Vowels getVowels(String inputText) throws TextAnalyzerException {
        Optional<String> optionalText = Optional.ofNullable(inputText);
        if (optionalText.isPresent()) {
            int numA = 0;
            int numE = 0;
            int numI = 0;
            int numO = 0;
            int numU = 0;
            char[] chars = optionalText.get().toCharArray();
            for (char aChar : chars) {
                if (aChar == 'a' || aChar == 'A')
                    numA++;
                if (aChar == 'e' || aChar == 'E')
                    numE++;
                if (aChar == 'i' || aChar == 'I')
                    numI++;
                if (aChar == 'o' || aChar == 'O')
                    numO++;
                if (aChar == 'u' || aChar == 'U')
                    numU++;
            }
            return new Vowels(numA, numE, numI, numO, numU);
        } else {
            throw new TextAnalyzerException("Input Text is Null");
        }
    }

    public Consonants getConsonants(String inputText) throws TextAnalyzerException {
        Optional<String> optionalText = Optional.ofNullable(inputText);
        if (optionalText.isPresent()) {
            String text = optionalText.get()
                    .replaceAll("[^A-Za-zäöüÄÖÜß]", "")
                    .toUpperCase();
            HashMap<String, Integer> consonants = new HashMap<>();
            char[] chars = text.toCharArray();
            for (int i = 0; i < chars.length; i++) {
                if (chars[i] != 'A'
                        && chars[i] != 'E'
                        && chars[i] != 'I'
                        && chars[i] != 'O'
                        && chars[i] != 'U'
                ) {
                    String stringCharacter = String.valueOf(chars[i]);
                    if (consonants.containsKey(stringCharacter)) {
                        int num = consonants.get(stringCharacter);
                        consonants.put(stringCharacter, ++num);
                    } else {
                        consonants.put(stringCharacter, 1);
                    }
                }
            }
            return new Consonants(consonants);
        } else {
            throw new TextAnalyzerException("Input Text is Null");
        }
    }
}
