package com.textanalyzer.textanalyzer.exception;

public class TextAnalyzerException extends Exception{
    public TextAnalyzerException(String errorMessage) {
        super(errorMessage);
    }
}
