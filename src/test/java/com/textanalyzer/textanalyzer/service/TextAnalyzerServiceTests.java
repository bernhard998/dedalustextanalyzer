package com.textanalyzer.textanalyzer.service;

import com.textanalyzer.textanalyzer.exception.TextAnalyzerException;
import com.textanalyzer.textanalyzer.model.Consonants;
import com.textanalyzer.textanalyzer.model.Vowels;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TextAnalyzerServiceTests {
    private final TextAnalyzerService textAnalyzerService = new TextAnalyzerService();

    @Test
    public void getVowels_Correct() throws TextAnalyzerException {
        //Arrange
        String inputText = "Lorem ipsum dolor sit amet";
        //Act
        Vowels resultVowels = textAnalyzerService.getVowels(inputText);
        //Assert
        Assertions.assertEquals(1, resultVowels.getA());
        Assertions.assertEquals(2, resultVowels.getE());
        Assertions.assertEquals(2, resultVowels.getI());
        Assertions.assertEquals(3, resultVowels.getO());
        Assertions.assertEquals(1, resultVowels.getU());
    }

    @Test
    public void getVowels_Correct_Empty_Text() throws TextAnalyzerException {
        //Arrange
        String inputText = "";
        //Act
        Vowels resultVowels = textAnalyzerService.getVowels(inputText);
        //Assert
        Assertions.assertEquals(0, resultVowels.getA());
        Assertions.assertEquals(0, resultVowels.getE());
        Assertions.assertEquals(0, resultVowels.getI());
        Assertions.assertEquals(0, resultVowels.getO());
        Assertions.assertEquals(0, resultVowels.getU());
    }

    @Test
    public void getVowels_NULL() {
        //Assert
        Assertions.assertThrows(TextAnalyzerException.class, () -> textAnalyzerService.getVowels(null));
    }

    @Test
    public void getConsonants_Correct() throws TextAnalyzerException {
        //Arrange
        String inputText = "Lorem ipsum dolor sit amet";
        //Act
        Consonants resultConsonants = textAnalyzerService.getConsonants(inputText);
        //Assert
        Assertions.assertEquals(2, resultConsonants.getConsonants().get("L"));
        Assertions.assertEquals(2, resultConsonants.getConsonants().get("R"));
        Assertions.assertEquals(3, resultConsonants.getConsonants().get("M"));
        Assertions.assertEquals(1, resultConsonants.getConsonants().get("P"));
        Assertions.assertEquals(2, resultConsonants.getConsonants().get("S"));
        Assertions.assertEquals(1, resultConsonants.getConsonants().get("D"));
        Assertions.assertEquals(2, resultConsonants.getConsonants().get("T"));
    }

    @Test
    public void getConsonants_Correct_Empty_Text() throws TextAnalyzerException {
        //Arrange
        String inputText = "";
        //Act
        Consonants resultConsonants = textAnalyzerService.getConsonants(inputText);
        //Assert
        Assertions.assertEquals(0, resultConsonants.getConsonants().size());
    }

    @Test
    public void getConsonants_NULL() {
        //Assert
        Assertions.assertThrows(TextAnalyzerException.class, () -> textAnalyzerService.getConsonants(null));
    }

    @Test
    public void getConsonants_Correct_Only_Characters() throws TextAnalyzerException {
        //Arrange
        String inputText = "1263967450465)&$(/§&(!&%=(&!?§)&?)!§$=?";
        //Act
        Consonants resultConsonants = textAnalyzerService.getConsonants(inputText);
        //Assert
        Assertions.assertEquals(0, resultConsonants.getConsonants().size());
    }
}
