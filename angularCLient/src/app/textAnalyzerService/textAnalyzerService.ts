import {Vowel} from "../model/vowel";
import {Consonants} from "../model/consonants";
import {Injectable} from "@angular/core";
@Injectable({
  providedIn: 'root'
})
export class TextAnalyzerService {
  public getVowels(text: string): Vowel {
    let numA = 0;
    let numE = 0;
    let numI = 0;
    let numO = 0;
    let numU = 0;
    for (let i = 0; i < text.length; i++) {
      if (text.charAt(i) == 'a' || text.charAt(i) == 'A')
        numA++;
      if (text.charAt(i) == 'e' || text.charAt(i) == 'E')
        numE++;
      if (text.charAt(i) == 'i' || text.charAt(i) == 'I')
        numI++;
      if (text.charAt(i) == 'o' || text.charAt(i) == 'O')
        numO++;
      if (text.charAt(i) == 'u' || text.charAt(i) == 'U')
        numU++;
    }
    return new Vowel(numA, numE, numI, numO, numU);
  }

  public getConsonants(text: String): Consonants {
    text = text
      .replace(/[^A-Za-zäöüÄÖÜß]/gm, '')
      .toUpperCase();
    let consonants = new Map<string, number>();
    for (let i = 0; i < text.length; i++) {
      if (text.charAt(i) != 'A'
        && text.charAt(i) != 'E'
        && text.charAt(i) != 'I'
        && text.charAt(i) != 'O'
        && text.charAt(i) != 'U'
      ) {
        let stringCharacter = text.charAt(i).valueOf();
        if (consonants.has(stringCharacter)) {
          let num = consonants.get(stringCharacter);
          // @ts-ignore
          consonants.set(stringCharacter, num += 1);
        } else {
          consonants.set(stringCharacter, 1);
        }
      }
    }
    return new Consonants(consonants);
  }
}
