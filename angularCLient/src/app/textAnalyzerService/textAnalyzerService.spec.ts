import {TestBed} from '@angular/core/testing';
import {TextAnalyzerService} from "./textAnalyzerService";


describe('ApiService', () => {
  let textAnalyzerService: TextAnalyzerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    textAnalyzerService = TestBed.inject(TextAnalyzerService);
  });

  it('Get Vowels Correct', () => {
    //Arrange
    let textInput = "Lorem ipsum dolor sit amet";
    //Assert
    let result = textAnalyzerService.getVowels(textInput);
    //Act
    expect(result.a).toBe(1);
    expect(result.e).toBe(2);
    expect(result.i).toBe(2);
    expect(result.o).toBe(3);
    expect(result.u).toBe(1);
  });

  it('Get Vowels Correct Empty String', () => {
    //Arrange
    let textInput = "";
    //Assert
    let result = textAnalyzerService.getVowels(textInput);
    //Act
    expect(result.a).toBe(0);
    expect(result.e).toBe(0);
    expect(result.i).toBe(0);
    expect(result.o).toBe(0);
    expect(result.u).toBe(0);
  });

  it('Get Consonants Correct', () => {
    //Arrange
    let textInput = "Lorem ipsum dolor sit amet";
    //Assert
    let result = textAnalyzerService.getConsonants(textInput);
    //Act
    expect(result.consonants.get("L")).toBe(2);
    expect(result.consonants.get("R")).toBe(2);
    expect(result.consonants.get("M")).toBe(3);
    expect(result.consonants.get("P")).toBe(1);
    expect(result.consonants.get("S")).toBe(2);
    expect(result.consonants.get("D")).toBe(1);
    expect(result.consonants.get("T")).toBe(2);
  });

  it('Get Consonants Correct Empty String', () => {
    //Arrange
    let textInput = "";
    //Assert
    let result = textAnalyzerService.getConsonants(textInput);
    //Act
    expect(result.consonants.size).toBe(0);
  });

  it('Get Consonants Correct Only Characters', () => {
    //Arrange
    let textInput = "1263967450465)&$(/§&(!&%=(&!?§)&?)!§$=?";
    //Assert
    let result = textAnalyzerService.getConsonants(textInput);
    //Act
    expect(result.consonants.size).toBe(0);
  });
});
