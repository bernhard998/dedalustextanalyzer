import {Component} from '@angular/core';
import {ApiService} from './service/api.service';
import {Vowel} from './model/vowel';
import {TextAnalyzerService} from './textAnalyzerService/textAnalyzerService';

import {Consonants} from "./model/consonants";
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  vowel: Vowel;
  consonants: Consonants;
  inputText = '';
  makeRestRequest = true;
  textAnalyzer: TextAnalyzerService;

  constructor(private api: ApiService) {
    this.vowel = new Vowel(0,0,0,0,0);
    this.consonants = new Consonants(new Map<string,number>());
    this.textAnalyzer = new TextAnalyzerService();
  }

  analyzeText(){
    if(this.makeRestRequest){
      //Online
      this.getVowels();
      this.getConsonants();
    }else {
      //Offline
      this.vowel = this.textAnalyzer.getVowels(this.inputText);
      this.consonants = this.textAnalyzer.getConsonants(this.inputText);
    }
  }

  getVowels() {
    this.api.getVowels(this.inputText)
      .subscribe(resp => {
        this.vowel = resp;
      },(error) => {                              //Error callback
      console.error('error while getting data from rest')
      });
  }

  getConsonants() {
    this.api.getConsonants(this.inputText)
      .subscribe(resp => {
        this.consonants = resp;
      },(error) => {                              //Error callback
        console.error('error while getting data from rest')
      });
  }
}
