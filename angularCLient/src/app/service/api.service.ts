import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpParams  } from '@angular/common/http';
const baseUrl = 'http://localhost:8080/';
const vowelsPath = 'vowels';
const consonantsPath = 'consonants';
import { Vowel } from '../model/vowel';
import {Observable, of} from 'rxjs';
import {Consonants} from "../model/consonants";


@Injectable({
  providedIn: 'root'
})

export class ApiService {

  constructor(private http: HttpClient) { }

  getVowels(input: string): Observable<Vowel> {
    let parameters = new HttpParams().set("text",input);
    return this.http.get<Vowel>(
      baseUrl+vowelsPath, {params:parameters}); }


  getConsonants(input: string): Observable<Consonants> {
    let parameters = new HttpParams().set("text",input);
    return this.http.get<Consonants>(
      baseUrl+consonantsPath, {params:parameters}); }

}

