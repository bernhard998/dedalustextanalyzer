export class Vowel {
  constructor(a: number, e: number, i: number, o: number, u: number) {
    this.a = a;
    this.e = e;
    this.i = i;
    this.o = o;
    this.u = u;
  }

  a: number;
  e: number;
  i: number;
  o: number;
  u: number;
}
